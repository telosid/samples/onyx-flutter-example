#import "AppDelegate.h"
#import "GeneratedPluginRegistrant.h"
#import "OnyxPlugin.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

      [GeneratedPluginRegistrant registerWithRegistry:self];
        FlutterViewController *flutterViewController =(FlutterViewController*)self.window.rootViewController;

        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.window.rootViewController = flutterViewController;
        [self.window makeKeyAndVisible];
        OnyxPlugin.flutterViewController = flutterViewController;

      // Override point for customization after application launch.
      return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

@end
